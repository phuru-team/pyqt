#!/usr/bin/env python

# embedding_in_qt4.py --- Simple Qt4 application embedding matplotlib canvases
#
# Copyright (C) 2005 Florent Rougon
#               2006 Darren Dale
#
# This file is an example program for matplotlib. It may be used and
# modified with no restriction; raw copies as well as modified versions
# may be distributed without limitation.

from __future__ import unicode_literals
import sys
import os
import random

# Importamos la libreira de PySerial
#import serial

#Importamos las librerias para 3D
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm



from matplotlib.backends import qt4_compat
use_pyside = qt4_compat.QT_API == qt4_compat.QT_API_PYSIDE
if use_pyside:
    from PySide import QtGui, QtCore
else:
    from PyQt4 import QtGui, QtCore

from numpy import arange, sin, pi
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

progname = os.path.basename(sys.argv[0])
progversion = "0.1"

#################################################################################
#Usando SOCKETs
import socket
import sys
from thread import *
 
HOST = '10.0.0.63'   # Symbolic name meaning all available interfaces
PORT = 8080             # Arbitrary non-privileged port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
    s.bind((HOST, PORT))
except socket.error , msg:
    print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
    sys.exit()
s.listen(10)
print 'Socket now listening'

#Function for handling connections. This will be used to create threads
def clientthread(conn):
    while True:
        data = conn.recv(1024)
        print data

        sDatos = data
        x,y,z,d=sDatos.split(",")
        
        xs = int(x)
        ys = int(y)
        zs = int(z)
        addVector (xs, ys, zs)

        if not data: 
            break
        #conn.sendall(reply)
     
    #came out of loop
    conn.close()
#################################################################################

# Abrimos el puerto del arduino a 9600
#PuertoSerie = serial.Serial('/dev/ttyUSB0', 9600)

Vec_x = []
Vec_y = []
Vec_z = []
limit = 0

def addVector(vx, vy, vz):
    global limit
    global Vec_x
    global Vec_y
    global Vec_z

    limit += 1
    Vec_x.append(vx)
    Vec_y.append(vy)
    Vec_z.append(vz)

class MyMplCanvas(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""
    def __init__(self, parent=None, width=5, height=4, dpi=100):

        fig = plt.figure()
        self.axes = fig.add_subplot(111, projection='3d')
        

        #fig = Figure(figsize=(width, height), dpi=dpi)
        #self.axes = fig.add_subplot(111)

        # We want the axes cleared every time plot() is called
        self.axes.hold(True)

        self.compute_initial_figure()

        #
        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtGui.QSizePolicy.Expanding,
                                   QtGui.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    def compute_initial_figure(self):
        pass


class MyStaticMplCanvas(MyMplCanvas):
    """Simple canvas with a sine plot."""
    def compute_initial_figure(self):
        t = arange(0.0, 3.0, 0.01)
        s = sin(2*pi*t)
        self.axes.plot(t, s)


class MyDynamicMplCanvas(MyMplCanvas):
    """A canvas that updates itself every second with a new plot."""
    def __init__(self, *args, **kwargs):
        MyMplCanvas.__init__(self, *args, **kwargs)
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update_figure)
        timer.start(1000)

    def compute_initial_figure(self):
        self.axes.plot([0, 1, 2, 3], [1, 1, 1, 1], 'r')

    def update_figure(self):
        # Build a list of 4 random integers between 0 and 10 (both inclusive)
        #l = [random.randint(0, 10) for i in range(4)]
        #self.axes.plot([0, 1, 2, 3], l, 'r')

        #Leyendo desde el puerto Serial
        #sDatos = PuertoSerie.readline()
        #sDatos = "1,1,1,1"
        #x,y,z,d=sDatos.split(",")
        
        #xs = int(x)
        #ys = int(y)
        #zs = int(z)
        #ds = int(d)

        #myList=[xs,ys,zs,ds]
        #self.axes.scatter(xs, ys, zs, c='r', marker='o')

        #addVector (xs, ys, zs)

        for i in range(1, limit):
            self.axes.plot([Vec_x[i], Vec_x[i-1]], [Vec_y[i], Vec_y[i-1]], zs=[Vec_z[i], Vec_z[i-1]])
            #self.axes.scatter(Vec_x[i], Vec_y[i], Vec_z[i], c='r', marker='o')
            #print  str(i) + " " + str(Vec_x[i]) + " - " + str(Vec_x[i-1])

        self.draw()


class ApplicationWindow(QtGui.QMainWindow):
    def __init__(self):

        QtGui.QMainWindow.__init__(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setWindowTitle("application main window")

        self.file_menu = QtGui.QMenu('&File', self)
        self.file_menu.addAction('&Quit', self.fileQuit,
                                 QtCore.Qt.CTRL + QtCore.Qt.Key_Q)
        self.menuBar().addMenu(self.file_menu)

        self.help_menu = QtGui.QMenu('&Help', self)
        self.menuBar().addSeparator()
        self.menuBar().addMenu(self.help_menu)

        self.help_menu.addAction('&About', self.about)

        self.main_widget = QtGui.QWidget(self)

        l = QtGui.QVBoxLayout(self.main_widget)
        sc = MyStaticMplCanvas(self.main_widget, width=5, height=4, dpi=100)
        dc = MyDynamicMplCanvas(self.main_widget, width=500, height=500, dpi=200)
#        l.addWidget(sc)
        l.addWidget(dc)

        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)


        #wait to accept a connection - blocking call
        conn, addr = s.accept()
        print 'Connected with ' + addr[0] + ':' + str(addr[1])
        #start new thread takes 1st argument as a function name to be run, second is the tuple of arguments to the function.
        start_new_thread(clientthread ,(conn,))


        self.statusBar().showMessage("All hail matplotlib!", 2000)

    def fileQuit(self):
        self.close()

    def closeEvent(self, ce):
        self.fileQuit()

    def about(self):
        QtGui.QMessageBox.about(self, "About",
"""embedding_in_qt4.py example
Copyright 2005 Florent Rougon, 2006 Darren Dale

This program is a simple example of a Qt4 application embedding matplotlib
canvases.

It may be used and modified with no restriction; raw copies as well as
modified versions may be distributed without limitation."""
)


qApp = QtGui.QApplication(sys.argv)

aw = ApplicationWindow()
aw.setWindowTitle("%s" % progname)
aw.show()
sys.exit(qApp.exec_())
#qApp.exec_()
