from socketIO_client import SocketIO, LoggingNamespace

def on_aaa_response(*args):
    print('on_aaa_response', args)

socketIO = SocketIO('10.0.0.34', 8080, LoggingNamespace)
socketIO.on('event', on_aaa_response)
socketIO.wait(seconds=1)
